import 'dart:async';
import 'dart:io' as io;
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = new DatabaseHelper.internal();

  factory DatabaseHelper() => _instance;

  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDb();
    return _db;
  }

  DatabaseHelper.internal();

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "main.db");
    var theDb = await openDatabase(path, version: 1, onCreate: _onCreate);
    return theDb;
  }

  void _onCreate(Database db, int version) async {
    // When creating the db, create the table
    await db.execute(
        "CREATE TABLE User(username TEXT, password TEXT)");
    print("Created tables");
    //After table creation add a demo user
    saveUser("admin", "admin");
  }

  Future<void> saveUser(String email, String password) async {
    //if User exist remove
    deleteUsers();
    var dbClient = await db;
    //Insert new user
    String sql =
        "INSERT INTO User (username,password) VALUES ('$email','$password')";
     await dbClient.rawQuery(sql);
  }

  Future<bool> loginUser(String email, String password) async {
    //if the user with email and password exist return true else false
  var dbClient = await db;
    print(email+" "+password);
    String sql = "SELECT * FROM User WHERE username = '$email' AND password = '$password'";
    var result = await dbClient.rawQuery(sql);
    print(result);

    if (result.length == 0) return false;
    return true;
  }

  Future<int> deleteUsers() async {
    //Remove User
    var dbClient = await db;
    int res = await dbClient.delete("User");
    return res;
  }

}
