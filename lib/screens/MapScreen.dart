import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  final Set<Marker> _markers = Set();
  final double _zoom = 16;//camera zoom value
  Completer<GoogleMapController> _controller = Completer();
  Position currentLocation;//position variable for current location
  LatLng _center;//latlong handler variable
  CameraPosition _initialPosition;
  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
//    after creating map show set default marker with current location
    _showHome(currentLocation);
    //add a store
    _addStore();
    //add a building
    _addBuilding();
  }

  @override
  void initState() {
    super.initState();
    //initially get user location
    getUserLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(75.0),
        child: AppBar(
          backgroundColor: Color.fromRGBO(243, 111, 33, 0.81),
          title: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Map'),
          ),
          centerTitle: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(60),
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          //while getting runtime permission _initialPosition will be null so adding a blank container
          _initialPosition != null
              ? GoogleMap(
                  markers: _markers,
                  mapType: MapType.normal,
                  onMapCreated: _onMapCreated,
                  initialCameraPosition: _initialPosition,
                  circles: Set.from([
                    Circle(
                        circleId: CircleId("circle"),
                        center: LatLng(_center.latitude, _center.longitude),
                        radius: 5000,
                        strokeWidth: 1,
                        fillColor: Colors.blueAccent.withOpacity(0.5))
                  ]),
                )
              : Container(),
        ],
      ),
    );
  }

  Future<Position> locateUser() async {
    //get current position
    return Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    currentLocation = await locateUser();
    setState(() {
      _center = LatLng(currentLocation.latitude, currentLocation.longitude);
      _initialPosition = CameraPosition(target: _center, zoom: _zoom);
    });
  }
//function for showing home location
  Future<void> _showHome(Position currentLocation) async {
    double lat = currentLocation.latitude;
    double long = currentLocation.longitude;
    GoogleMapController controller = await _controller.future;
    controller
        .animateCamera(CameraUpdate.newLatLngZoom(LatLng(lat, long), _zoom));
    setState(() {
      _markers.clear();
      _markers.add(
        Marker(
            markerId: MarkerId('Home'),
            position: LatLng(lat, long),
            infoWindow: InfoWindow(
                title: 'Home', snippet: 'This is your current location')),
      );
    });
  }
//function for showing store location
  Future<void> _addStore() async {
    double lat = 8.5166348;
    double long = 76.941053;
    var image = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(16, 16)),
      "assets/shop.png",
    );
    setState(() {
      _markers.add(
        Marker(
            icon: image,
            markerId: MarkerId('newstore'),
            position: LatLng(lat, long),
            infoWindow:
                InfoWindow(title: 'New Store', snippet: 'Welcome to NewStore')),
      );
    });
  }
//function for showing building location
  Future<void> _addBuilding() async {
    double lat = 8.5019339;
    double long = 76.9551863;
    var image = await BitmapDescriptor.fromAssetImage(
      ImageConfiguration(size: Size(16, 16)),
      "assets/shop2.png",
    );
    setState(() {
      _markers.add(
        Marker(
            icon: image,
            markerId: MarkerId('sam'),
            position: LatLng(lat, long),
            infoWindow: InfoWindow(
                title: 'Sam Tower', snippet: 'Welcome to Sam Tower')),
      );
    });
  }
}
