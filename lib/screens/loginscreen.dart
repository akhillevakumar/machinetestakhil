import 'package:flutter/material.dart';
import 'package:geolocation/db/DatabaseHelper.dart';
import 'package:geolocation/screens/MapScreen.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();//Scafold key
  TextEditingController _emailController = TextEditingController();//controller for get text from email field
  TextEditingController _passwordController = TextEditingController();//controller for get text from password field
  DatabaseHelper db = new DatabaseHelper();
  var text1 = "Login";//text for App bar
  var text2 = "to Continue";//text for App bar
  var errorText = "";//text for error
  bool _isLogedin = false;//boolean for changing ui

  //Color variables
  Color orange = Color.fromRGBO(243, 111, 33, 0.81);
  Color green = Color.fromRGBO(26, 173, 150, 0.81);
  Color darkBlue = Color.fromRGBO(37, 33, 242, 0.81);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: Container(
        color: Colors.white10,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            ClipRRect(
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(200)),
              child: ClipRRect(
                borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(200)),
                child: Container(
                    width: double.infinity,
                    height: 240,
                    color: orange,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            text1,
                            style: TextStyle(color: Colors.white, fontSize: 35),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8),
                            child: Text(
                              text2,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 35),
                            ),
                          ),
                        ],
                      ),
                    )),
              ),
            ),
            !_isLogedin ? _loginCard() : _loadButton(),//if not loggedIn Show login form else show button for navigate to map
            //bottom green widget
            ClipRRect(
              borderRadius: BorderRadius.only(topLeft: Radius.circular(150)),
              child: Container(
                height: 120,
                width: double.infinity,
                color: green,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
//Button for Navigating to Map
  Widget _loadButton() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.only(
                topLeft: Radius.circular(18), bottomRight: Radius.circular(18)),
            side: BorderSide(color: darkBlue)),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MapScreen()),
          );
        },
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                'Show My Current Location',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  color: darkBlue,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.location_on,
                color: darkBlue,
              ),
            )
          ],
        ),
      ),
    );
  }
//Login form
  Widget _loginCard() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
              child: Text(
                errorText,
                style: TextStyle(color: Colors.redAccent),
              )),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: TextFormField(
              controller: _emailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: "Username",
                prefixIcon: Icon(
                  Icons.person,
                  color: darkBlue,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
            child: TextFormField(
              controller: _passwordController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: "Password",
                prefixIcon: Icon(
                  Icons.lock,
                  color: darkBlue,
                ),
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 50),
              child: FloatingActionButton(
                onPressed: () async {
                  //On submit button pressed check user from db
                  var status = await db.loginUser(
                      _emailController.text, _passwordController.text);
                  if (status) {
                    print("true");
                    //if user exist update UI
                    setState(() {
                      _isLogedin = true;
                      text1 = "Continue to";
                      text2 = "Map";
                      errorText = "";
                    });
                  } else {
                    //if user not exist show error text 
                    print("error");
                    setState(() {
                      errorText = "Incorrect username or password";
                    });
                  }
                },
                child: Icon(Icons.arrow_forward_ios),
                backgroundColor: darkBlue,
                elevation: 8,
              )),
        ],
      ),
    );
  }
}
